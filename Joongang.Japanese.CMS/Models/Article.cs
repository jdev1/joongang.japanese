﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joongang.Japanese.CMS.Models
{
    /// <summary>
    /// 아티클 아이템
    /// </summary>
    public class Article
    {
        public Int32? aid { get; set; }
        public String title { get; set; }
        public String pnews { get; set; }
        public String bulkflag { get; set; }
        public String subtitle { get; set; }
        public String inputtime { get; set; }
        public String inputdate { get; set; }
        public String sourcecode { get; set; }
        public Int32? viewcnt { get; set; }
        public String servcode { get; set; }
        public String sectcode { get; set; }
        public String com_flag { get; set; }
        public String mnews { get; set; }
        public String mnews_link { get; set; }
        public String tag { get; set; }
        public String edit_img { get; set; }
        public Int32? revision { get; set; }
        public String updatedate { get; set; }
        public String updatetime { get; set; }
        public String xml_link { get; set; }
        public String translator { get; set; }
        public String content { get; set; }
        public String content_etc { get; set; }
        public String content_kor { get; set; }
        public Int64? rownumber { get; set; }
        public String sourcecodeName
        {
            get
            {
                string text = "";
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic = Joongang.Japanese.Repository.Common.InitData.GetSourceCode();
                text = (!string.IsNullOrEmpty(this.sourcecode))? dic[this.sourcecode] : "";
                return text;
            }
        }
    }

    public class ArticleWrite
    {
        /// <summary> 출처</summary>
        public Dictionary<string, string> ArticleSourceCode { get; set; }
        /// <summary> 기사데이터(신규등록인 경우 null) </summary>
        public Article Data { get; set; }
        /// <summary>수정모드여부</summary>
        public bool IsEditMode { get; set; }
    }

    public class RelateArticle
    {
        public String name { get; set; }
        public Int32? cid { get; set; }
    }

    public class RelateArticleWrite
    {
        public RelateArticle Data { get; set; }
        public bool IsEditMode { get; set; }
    }
}