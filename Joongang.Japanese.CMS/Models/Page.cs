﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joongang.Japanese.CMS.Models
{
    public class Page<TModel>
    {
        /// <summary>
        /// 현재 페이지 번호 (기본값 1)
        /// </summary>
        public int PageNum { get; set; } = 1;
        /// <summary>
        /// 총 아이템 갯수 (기본값 0)
        /// </summary>
        public int TotalItemCount { get; set; } = 0;
        /// <summary>
        /// 화면에 표시할 아이템 갯수 (기본값 10)
        /// </summary>
        public int DisplayItemCount { get; set; } = 10;
        /// <summary>
        /// 화면에 표시할 페이지 수 (기본값 10)
        /// </summary>
        public int DisplayPageCount { get; set; } = 10;
        /// <summary>
        /// 데이터
        /// </summary>
        public IEnumerable<TModel> Data { get; set; }
    }
}