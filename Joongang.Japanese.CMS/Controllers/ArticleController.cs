﻿using System;
using System.Web.Mvc;

namespace Joongang.Japanese.CMS.Controllers
{
    public class ArticleController : Controller
    {
        // GET: Article
        public ActionResult Index(int page = 1, string search_value = "")
        {
            var total_rows = 0;
            var entity = new Joongang.Japanese.CMS.Models.Page<Joongang.Japanese.CMS.Models.Article> { PageNum = page };
            entity.Data = Joongang.Japanese.CMS.Repository.ArticleRepository.GetArticleList(entity.PageNum, entity.DisplayItemCount, search_value, out total_rows);
            entity.TotalItemCount = total_rows;

            return View(entity);
        }


        public ActionResult Write(Int32? aid)
        {
            var entity = new Joongang.Japanese.CMS.Models.ArticleWrite() { IsEditMode = aid.HasValue };
            entity.ArticleSourceCode = Joongang.Japanese.Repository.Common.InitData.GetSourceCode();

            if (aid.HasValue)
            {
                entity.Data = Joongang.Japanese.CMS.Repository.ArticleRepository.Detail(aid.Value);
            }else
            {
                entity.Data = new Joongang.Japanese.CMS.Models.Article();
            }

            return View(entity);
        }

        /// <summary>
        /// 등록/수정
        /// </summary>
        /// <param name="wiki">엔티티</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Write(Joongang.Japanese.CMS.Models.Article article)
        {
            if (article != null)
            {
                Joongang.Japanese.CMS.Repository.ArticleRepository.Insert(article);
                return Json(true);
            }
            return Json(false);
        }
    }
}