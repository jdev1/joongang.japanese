﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joongang.Japanese.CMS.Controllers
{
    public class HelperController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string query)
        {
            return View(GetEntityProperties(query));
        }

        [NonAction]
        private IEnumerable<string> GetEntityProperties(string query)
        {
            var dtList = new List<System.Data.DataTable>();
            var items = new List<string>();

            using (var db = Joongang.Japanese.Repository.Common.BaseRepository.GetDbContext())
            {
                using (var cmd = db.Database.Connection.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.CommandType = System.Data.CommandType.Text;

                    try
                    {
                        db.Database.Connection.Open();

                        using (var reader = cmd.ExecuteReader(System.Data.CommandBehavior.SchemaOnly))
                        {
                            while (!reader.IsClosed)
                            {
                                var dt = new System.Data.DataTable();
                                dt.Load(reader);
                                dtList.Add(dt);
                            }
                        }
                    }
                    finally
                    {
                        try
                        {
                            db.Database.Connection.Close();
                        }
                        catch { }
                    }
                }
            }

            foreach (var dt in dtList)
            {
                items.Add("==> TABLE#" + (dtList.IndexOf(dt) + 1) + "<br/>");
                foreach (System.Data.DataColumn col in dt.Columns)
                {
                    var colType = col.DataType;
                    var colName = col.ColumnName.ToLower();
                    var isAllowNull = col.AllowDBNull;

                    items.Add("public " + colType.Name + (colType != typeof(string) ? "? " : " ") + colName + " { get; set; }");
                }

                items.Add("==================");
            }

            return items;
        }
    }
}