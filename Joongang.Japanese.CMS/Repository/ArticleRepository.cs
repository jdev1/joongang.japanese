﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Joongang.Japanese.CMS.Repository
{
    public class ArticleRepository
    {
        /// <summary>
        /// 기사 저장하기
        /// </summary>
        public static void Insert(Joongang.Japanese.CMS.Models.Article article)
        {
            var args = new IDbDataParameter[] {
                new SqlParameter { ParameterName = "@aid", Value = article.aid ?? 0 },
                new SqlParameter { ParameterName = "@title", Value = article.title },
                new SqlParameter { ParameterName = "@pnews", Value = article.pnews },
                new SqlParameter { ParameterName = "@content", Value = article.content },
                new SqlParameter { ParameterName = "@content_etc", Value = article.content_etc },
                new SqlParameter { ParameterName = "@content_kor", Value = article.content_kor },
                new SqlParameter { ParameterName = "@translator", Value = article.translator },
                new SqlParameter { ParameterName = "@sourcecode", Value = article.sourcecode }
            };

            Joongang.Japanese.Repository.Common.BaseRepository.ExecuteNonQueryFromProcedure("USP_ADMIN_ARTICLE_INS", args);
        }

        /// <summary>
        /// 기사 상세 내용 불러오기
        /// </summary>
        public static Joongang.Japanese.CMS.Models.Article Detail(int aid)
        {
            var args = new IDbDataParameter[]
            {
                new SqlParameter { ParameterName = "@aid", Value = aid }
            };

            return Joongang.Japanese.Repository.Common.BaseRepository.GetSingleDataFromProcedure<Joongang.Japanese.CMS.Models.Article>("USP_ADMIN_ARTICLE_DETAIL", args);
        }

        /// <summary>
        /// 위키 목록을 조회합니다.
        /// </summary>
        /// <param name="page">페이지 번호</param>
        /// <param name="page_size">페이지당 항목 갯수</param>
        /// <param name="search">검색어</param>
        /// <param name="total_size">[OUT] 전체 목록 갯수</param>
        /// <returns></returns>
        public static IEnumerable<Joongang.Japanese.CMS.Models.Article> GetArticleList(int page, int page_size, string search, out int total_size)
        {
            var args = new IDbDataParameter[] {
                new SqlParameter { ParameterName = "@PAGE", Value = page },
                new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                new SqlParameter { ParameterName = "@SEARCH", Value = search },
                new SqlParameter { ParameterName = "@TOTAL_SIZE", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output }
            };

            var result = Joongang.Japanese.Repository.Common.BaseRepository.GetDataFromProcedure<Joongang.Japanese.CMS.Models.Article>("USP_ADMIN_ARTICLE_LIST", args);
            total_size = Joongang.Japanese.Repository.Common.BaseRepository.GetOutputValue<int>(args, "@TOTAL_SIZE");

            return result;
        }
    }
}