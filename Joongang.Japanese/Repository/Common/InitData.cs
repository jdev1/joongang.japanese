﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joongang.Japanese.Repository.Common
{
    public class InitData
    {
        public static Dictionary<string, string> GetSourceCode()
        {
            Dictionary<string, string> sourcecodes = new Dictionary<string, string>()
            { 
                { "1", "中央日報"},
                { "2", "中央日報日本語版"},
                { "3", "ISPlus"},
                { "4", "JES"},
                { "5", "アシア経済"},
                { "6", "OSEN"},
                { "7", "KONEST"},
                { "8", "ⓒ聯合ニュース"},
                { "9", "中央ＳＵＮＤＡＹ"},
                { "10", "韓国経済新聞社"}
            };
            return sourcecodes;
        }
    }
}
